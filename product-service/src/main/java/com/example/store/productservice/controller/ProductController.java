package com.example.store.productservice.controller;

import com.example.store.commons.dto.ProductDto;
import com.example.store.productservice.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @GetMapping("/{productId}")
    Mono<ProductDto> getProduct(@PathVariable("productId") String productId) {
        return productService.getProduct(productId);
    }

}
