package com.example.store.productservice.service;

import com.example.store.commons.dto.AdidasProductDto;
import com.example.store.commons.dto.AvgScoreResp;
import com.example.store.commons.dto.ProductDto;
import com.example.store.commons.restclient.AdidasProductApi;
import com.example.store.commons.restclient.ScoreApi;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final AdidasProductApi adidasProductApi;
    private final ScoreApi scoreApi;

    public Mono<ProductDto> getProduct(String productId) {
        Mono<AvgScoreResp> avgScoreRespMono = scoreApi.calculateAvgScore(productId);
        Mono<AdidasProductDto> productMono = adidasProductApi.getProduct(productId);
        return Mono.zip(avgScoreRespMono, productMono)
                .map(tuple -> ProductDto.builder()
                        .avgScore(tuple.getT1())
                        .adidasProductDto(tuple.getT2())
                        .build());
    }

}
