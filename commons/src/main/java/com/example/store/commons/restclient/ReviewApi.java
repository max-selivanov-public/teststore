package com.example.store.commons.restclient;

import com.example.store.commons.dto.CreateReviewReq;
import com.example.store.commons.dto.ReviewDto;
import com.example.store.commons.dto.UpdateReviewReq;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@ReactiveFeignClient(name="review-api", url="${api.store.review.base-url}", path = "/review")
public interface ReviewApi {

    @GetMapping
    Flux<ReviewDto> getAllReviews();

    @GetMapping("/{id}")
    Mono<ReviewDto> getReviewById(@PathVariable("id") UUID id);

    @PostMapping
    Mono<ReviewDto> createReview(@RequestBody CreateReviewReq createRequest);

    @PutMapping("/{id}")
    Mono<ReviewDto> updateReview(@PathVariable("id") UUID id, @RequestBody UpdateReviewReq updateRequest);
    
    @DeleteMapping("/{id}")
    Mono<Void> deleteReviewById(@PathVariable("id") UUID id);


}
