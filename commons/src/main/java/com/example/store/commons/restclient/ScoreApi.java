package com.example.store.commons.restclient;

import com.example.store.commons.dto.AvgScoreResp;
import com.example.store.commons.dto.CreateReviewReq;
import com.example.store.commons.dto.ReviewDto;
import com.example.store.commons.dto.UpdateReviewReq;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@ReactiveFeignClient(name="score-api", url="${api.store.score.base-url}", path = "/score")
public interface ScoreApi {

    @GetMapping("/{productId}")
    Mono<AvgScoreResp> calculateAvgScore(@PathVariable("productId") String productId);

}
