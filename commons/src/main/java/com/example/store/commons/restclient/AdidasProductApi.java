package com.example.store.commons.restclient;

import com.example.store.commons.dto.AdidasProductDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactivefeign.spring.config.ReactiveFeignClient;
import reactor.core.publisher.Mono;

@ReactiveFeignClient(name="adidas-product-api", url="${api.adidas.product.base-url}")
public interface AdidasProductApi {

    @GetMapping("/{productId}")
    Mono<AdidasProductDto> getProduct(@PathVariable("productId") String productId);

}
