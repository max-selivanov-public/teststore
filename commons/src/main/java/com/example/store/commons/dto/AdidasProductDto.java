package com.example.store.commons.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdidasProductDto {
    boolean sale;
    String brand;
    String color;
    String gender;
    boolean outlet;
    List<String> sport;
    List<String> closure;
    List<String> surface;
    String category;
    List<String> sportSub;
    List<String> collection;
    List<String> productType;
    List<String> technologies;
    boolean personalizable;
    boolean isCnCRestricted;
    boolean customizable;
    boolean isWaitingRoomProduct;
    boolean isInPreview;
    boolean specialLaunch;
}

