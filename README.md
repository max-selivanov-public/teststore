### Requiriments
- Java 18
- Maven 3.8.5
- Docker 20.10.12

### Installation
In root project folder run
```bash
mvn clean install
```
Then
```bash
docker-compose -f docker-compose.yaml up -d
```
`review-service` will fail to start because `cassandra` is starting up slowly and unavailable at the `review-service` start.
Wait for a couple of minutes for `cassandra` to start, and then run
```bash
docker start review-service
```
All services should be up and running now

### Test deployment
Run a `curl`
```bash
curl -X GET http://localhost:8090/product/BB5476
```

### Room for improvements
- Exception Handling
- Logging
- Tests, Unit5 + International
- Redis cache
- JWT Authentication
- And more