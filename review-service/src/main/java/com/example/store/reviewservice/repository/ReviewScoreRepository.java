package com.example.store.reviewservice.repository;

import com.example.store.reviewservice.model.ReviewScore;
import com.example.store.reviewservice.model.ReviewScoreKey;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewScoreRepository extends ReactiveCassandraRepository<ReviewScore, ReviewScoreKey> {

}
