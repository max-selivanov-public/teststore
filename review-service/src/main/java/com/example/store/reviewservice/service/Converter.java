package com.example.store.reviewservice.service;

import com.example.store.commons.dto.AvgScoreResp;
import com.example.store.commons.dto.ReviewDto;
import com.example.store.reviewservice.model.AvgScore;
import com.example.store.reviewservice.model.Review;
import com.example.store.reviewservice.model.ReviewScore;
import com.example.store.reviewservice.model.ReviewScoreKey;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Converter {

    private final ModelMapper mapper;

    public ReviewScore toReviewScore(Review review) {
        var key = ReviewScoreKey.builder()
                .productId(review.getProductId())
                .reviewId(review.getId())
                .build();
        var reviewScore = ReviewScore.builder()
                .key(key)
                .score(review.getScore())
                .build();
        return reviewScore;
    }

    public ReviewScoreKey toReviewScoreKey(Review review) {
        return ReviewScoreKey.builder()
                .productId(review.getProductId())
                .reviewId(review.getId())
                .build();
    }

    public AvgScoreResp toAvgScoreResp(String productId, AvgScore avgScore) {
        return AvgScoreResp.builder()
                .productId(productId)
                .avgScore(avgScore.getAvgScore())
                .count(avgScore.getCount())
                .build();
    }

    public ReviewDto toReviewDto(Review review) {
        return mapper.map(review, ReviewDto.class);
    }
}
