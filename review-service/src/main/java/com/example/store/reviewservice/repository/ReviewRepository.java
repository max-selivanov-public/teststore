package com.example.store.reviewservice.repository;

import com.example.store.reviewservice.model.Review;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewRepository extends ReactiveCassandraRepository<Review, UUID> {

}
