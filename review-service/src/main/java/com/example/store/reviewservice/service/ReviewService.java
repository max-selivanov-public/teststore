package com.example.store.reviewservice.service;

import com.example.store.reviewservice.model.Review;
import com.example.store.reviewservice.model.ReviewScore;
import com.example.store.reviewservice.repository.ReviewRepository;
import com.example.store.reviewservice.repository.ReviewScoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ReviewService {

    private final Converter converter;
    private final ReviewRepository reviewRepository;
    private final ReviewScoreRepository reviewScoreRepository;

    public Flux<Review> getAllReviews() {
        return reviewRepository.findAll()
                .switchIfEmpty(Flux.empty());
    }

    public Mono<Review> getReviewById(UUID id) {
        return reviewRepository.findById(id)
                .switchIfEmpty(Mono.empty());
    }

    public Mono<Review> createReview(String productId, String userName, Integer score, String comment) {
        var review = Review.builder()
                .id(UUID.randomUUID())
                .productId(productId)
                .userName(userName)
                .score(score)
                .comment(comment)
                .build();
        return Mono.just(review)
                .flatMap(this::createReviewScore)
                .then(reviewRepository.save(review));
    }

    private Mono<ReviewScore> createReviewScore(Review review) {
        ReviewScore reviewScore = converter.toReviewScore(review);
        return reviewScoreRepository.save(reviewScore);
    }

    public Mono<Void> deleteReviewById(UUID reviewId) {
        return reviewRepository.findById(reviewId)
                .map(converter::toReviewScoreKey)
                .flatMap(reviewScoreRepository::deleteById)
                .then(reviewRepository.deleteById(reviewId));
    }

    public Mono<Review> updateReview(UUID reviewId, Integer score, String comment) {
        return reviewRepository.findById(reviewId)
                .map(converter::toReviewScoreKey)
                .flatMap(reviewScoreRepository::findById)
                .map(reviewScore -> {
                    reviewScore.setScore(score);
                    return reviewScore;
                })
                .flatMap(reviewScoreRepository::save)
                .then(reviewRepository.findById(reviewId))
                .map(review -> {
                    review.setScore(score);
                    review.setComment(comment);
                    return review;
                })
                .flatMap(reviewRepository::save);
    }

}
