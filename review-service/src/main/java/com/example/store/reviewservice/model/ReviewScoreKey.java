package com.example.store.reviewservice.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@PrimaryKeyClass
public class ReviewScoreKey {

    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    String productId;

    @PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED)
    UUID reviewId;

}