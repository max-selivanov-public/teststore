package com.example.store.reviewservice.controller;

import com.example.store.commons.dto.AvgScoreResp;
import com.example.store.commons.restclient.ScoreApi;
import com.example.store.reviewservice.service.Converter;
import com.example.store.reviewservice.service.ScoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/score")
public class ScoreController implements ScoreApi {

    private final ScoreService scoreService;
    private final Converter converter;

    public Mono<AvgScoreResp> calculateAvgScore(@PathVariable String productId) {
        return scoreService.calculateAvgScore(productId)
                .map(avgScore -> converter.toAvgScoreResp(productId, avgScore));
    }

}
