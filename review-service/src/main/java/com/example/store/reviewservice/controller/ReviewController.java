package com.example.store.reviewservice.controller;

import com.example.store.commons.dto.CreateReviewReq;
import com.example.store.commons.dto.ReviewDto;
import com.example.store.commons.dto.UpdateReviewReq;
import com.example.store.commons.restclient.ReviewApi;
import com.example.store.reviewservice.model.Review;
import com.example.store.reviewservice.service.Converter;
import com.example.store.reviewservice.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/review")
public class ReviewController implements ReviewApi {

    private final ReviewService reviewService;
    private final Converter converter;

    @Override
    public Flux<ReviewDto> getAllReviews() {
        Flux<Review> reviewFlux = reviewService.getAllReviews();
        return reviewFlux.map(converter::toReviewDto);
    }

    @Override
    public Mono<ReviewDto> getReviewById(@PathVariable UUID id) {
        Mono<Review> reviewMono = reviewService.getReviewById(id);
        return reviewMono.map(converter::toReviewDto);
    }

    @Override
    public Mono<ReviewDto> createReview(@RequestBody CreateReviewReq createRequest) {
        Mono<Review> reviewMono = reviewService.createReview(
                createRequest.getProductId(),
                createRequest.getUserName(),
                createRequest.getScore(),
                createRequest.getComment());
        return reviewMono.map(converter::toReviewDto);
    }

    @Override
    public Mono<ReviewDto> updateReview(@PathVariable UUID id, @RequestBody UpdateReviewReq updateRequest) {
        Mono<Review> reviewMono = reviewService.updateReview(id, updateRequest.getScore(), updateRequest.getComment());
        return reviewMono.map(converter::toReviewDto);
    }

    @Override
    public Mono<Void> deleteReviewById(@PathVariable UUID id) {
        return reviewService.deleteReviewById(id);
    }

}
