package com.example.store.reviewservice.repository;

import com.example.store.reviewservice.model.AvgScore;
import lombok.AllArgsConstructor;
import org.springframework.data.cassandra.core.cql.BeanPropertyRowMapper;
import org.springframework.data.cassandra.core.cql.ReactiveCqlOperations;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
@AllArgsConstructor
public class CustomRepository {

    private static final String CALC_COUNT_AND_AVG = "select count(*) as count, avg(cast(score AS float)) as avgscore from reviewscore where productid=?";
    private ReactiveCqlOperations operations;

    public Mono<AvgScore> calculateCountAndScore(String productId) {
        return operations.queryForObject(CALC_COUNT_AND_AVG, new BeanPropertyRowMapper<>(AvgScore.class), productId);
    }
}
