package com.example.store.reviewservice.service;

import com.example.store.reviewservice.model.AvgScore;
import com.example.store.reviewservice.repository.CustomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ScoreService {

    private final CustomRepository customRepository;

    public Mono<AvgScore> calculateAvgScore(String productId) {
        return customRepository.calculateCountAndScore(productId);
    }

}
